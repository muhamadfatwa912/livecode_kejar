class CreatePayments < ActiveRecord::Migration[6.0]
  def up
    create_table :payments do |t|
      t.string :id_transtaction
      t.string :status
      t.text :upload

      t.timestamps
    end
  end
  def down 
    drop_table :payments
  end
end
