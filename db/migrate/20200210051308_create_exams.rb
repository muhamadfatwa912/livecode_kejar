class CreateExams < ActiveRecord::Migration[6.0]
  def up
    create_table :exams do |t|
      t.string :title
      t.string :mapel
      t.integer :duration
      t.integer :nilai
      t.text :status
      t.string :label
      t.integer :student_id

      t.timestamps
    end
  end
  def down 
    drop_table :exams
  end
end
