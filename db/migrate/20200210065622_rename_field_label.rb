class RenameFieldLabel < ActiveRecord::Migration[6.0]
  def change
    rename_column :exams, :label, :level
  end
end
