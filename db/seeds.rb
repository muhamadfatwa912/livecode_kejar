# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Student.create(name: 'islam', username: "is1", age: 1, kelas: 'RPL', address: 'Bogor', city: 'Bogor', nik: '879878534')
Student.create(name: 'kristen', username: 'kris1', age: 90, kelas: 'TKJ', address: 'Bogor', city: 'Bogor', nik: '879878539')
Student.create(name: 'budha', username: 'bud1', age: 5, kelas: 'BDP', address: 'Bogor', city: 'Bogor', nik: '879878538')
Student.create(name: 'kong hu cu', username: 'kong1', age: 8, kelas: 'TBG', address: 'Bogor', city: 'Bogor', nik: '879878532')
Student.create(name: 'hindu', username: 'hin1', age: 9, kelas: 'MMD', address: 'Bogor', city: 'Bogor', nik: '8798785311')
Student.create(name: 'dudi', username: "is1", age: 1, kelas: 'RPL', address: 'Bogor', city: 'Bogor', nik: '879878534')
Student.create(name: 'sueb', username: 'kris1', age: 90, kelas: 'TKJ', address: 'Bogor', city: 'Bogor', nik: '879878539')
Student.create(name: 'kelju', username: 'bud1', age: 5, kelas: 'BDP', address: 'Bogor', city: 'Bogor', nik: '879878538')
Student.create(name: 'iba', username: 'kong1', age: 8, kelas: 'TBG', address: 'Bogor', city: 'Bogor', nik: '879878532')
Student.create(name: 'eki', username: 'hin1', age: 9, kelas: 'MMD', address: 'Bogor', city: 'Bogor', nik: '8798785311')
Exam.create(title: 'kosong', mapel: "is1", duration: 1, nilai: '100', status: 'Luncur', level: 1, student_id: '8')
Exam.create(title: 'kosong', mapel: 'kris1', duration: 90, nilai: '100', status: 'Luncur', level: 1, student_id: '9')
Exam.create(title: 'kosong', mapel: 'bud1', duration: 5, nilai: '1000', status: 'Luncur', level: 1, student_id: '7')
Exam.create(title: 'kosong', mapel: 'kong1', duration: 8, nilai: '100', status: 'Luncur', level: 1, student_id: '3')
Exam.create(title: 'kosong', mapel: 'hin1', duration: 9, nilai: '100', status: 'Luncur', level: 1, student_id: '4')