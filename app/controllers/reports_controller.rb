class ReportsController < ApplicationController
  def new
    @report = Report.new
end

def create
    report = Report.new(report_params)
    report.save
    flash[:notice] = "report has been created"
    redirect_to reports_path
end

def index
    @report = Report.all
end

def edit
    @report = Report.find(params[:id])
end

def update
    @report = Report.find(params[:id])
    @report.update(report_params)
    flash[:notice] = "report has been updated"
    redirect_to report_path(@report)
end

def destroy
  @report = Report.find(params[:id])
  @report.destroy
  redirect_to reports_path
  flash[:notice] = "Berhasil dihapus"
end

  def show
    @report = Report.find(params[:id])
  end
  

end

  private
  def report_params
    params.require(:report).permit(
      :title, :hasil, :mapel, :teacher_id, :student_id, :date
    )
  end