class TeachersController < ApplicationController
  def new
    @teacher = Teacher.new
end

def create
    teacher = Teacher.new(teacher_params)
    teacher.save
    flash[:notice] = "teacher has been created"
    redirect_to teachers_path
end

def index
    @teacher = Teacher.all
end

def edit
    @teacher = Teacher.find(params[:id])
end

def update
    @teacher = Teacher.find(params[:id])
    @teacher.update(teacher_params)
    flash[:notice] = "teacher has been updated"
    redirect_to teacher_path(@teacher)
end

def destroy
  @teacher = Teacher.find(params[:id])
  @teacher.destroy
  redirect_to teachers_path
  flash[:notice] = "Berhasil dihapus"
end

  def show
    @teacher = Teacher.find(params[:id])
  end
  

end

  private
  def teacher_params
    params.require(:teacher).permit(
      :nik, :name, :age, :kelas, :mapel
    )
  end