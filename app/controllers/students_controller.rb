class StudentsController < ApplicationController
  def new
    @student = Student.new
end

def create
    student = Student.new(student_params)
    student.save
    flash[:notice] = "Student has been created"
    redirect_to students_path
end

def index
    @student = Student.all
end

def edit
    @student = Student.find(params[:id])
end

def update
    @student = Student.find(params[:id])
    @student.update(student_params)
    flash[:notice] = "Student has been updated"
    redirect_to student_path(@student)
end

def destroy
  @student = Student.find(params[:id])
  @student.destroy
  redirect_to students_path
  flash[:notice] = "Berhasil dihapus"
end

  def show
    @student = Student.find(params[:id])
  end
  

end

  private
  def student_params
    params.require(:student).permit(:name, :username, :age, :kelas, :address, :city, :nik)
  end