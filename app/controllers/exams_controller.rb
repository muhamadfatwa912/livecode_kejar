class ExamsController < ApplicationController
  def new
    @exam = Exam.new
end

def create
    exam = Exam.new(exam_params)
    exam.save
    flash[:notice] = "exam has been created"
    redirect_to exams_path
end

def index
    @exam = Exam.all
end

def edit
    @exam = Exam.find(params[:id])
end

def update
    @exam = Exam.find(params[:id])
    @exam.update(exam_params)
    flash[:notice] = "exam has been updated"
    redirect_to exam_path(@exam)
end

def destroy
  @exam = Exam.find(params[:id])
  @exam.destroy
  redirect_to exams_path
  flash[:notice] = "Berhasil dihapus"
end

  def show
    @exam = Exam.find(params[:id])
  end
  

end

  private
  def exam_params
    params.require(:exam).permit(
      :title, :mapel, :duration, :nilai, :status, :level, :student_id
    )
  end