class PaymentsController < ApplicationController
  def new
    @payment = Payment.new
end

def create
    payment = Payment.new(payment_params)
    payment.save
    flash[:notice] = "payment has been created"
    redirect_to payments_path
end

def index
    @payment = Payment.all
end

def edit
    @payment = Payment.find(params[:id])
end

def update
    @payment = Payment.find(params[:id])
    @payment.update(payment_params)
    flash[:notice] = "payment has been updated"
    redirect_to payment_path(@payment)
end

def destroy
  @payment = Payment.find(params[:id])
  @payment.destroy
  flash[:notice] = "Berhasil dihapus"
  redirect_to payments_path
  
end

def show
    @payment = Payment.find(params[:id])
end
  

end

  private
  def payment_params
    params.require(:payment).permit(
      :id_transtaction, :status, :upload
    )
  end